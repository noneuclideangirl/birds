﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject bird;
    public float followSpeed = 5;
    
    private float zOffset;
    private float yOffset;

    private void Start() {
        zOffset = transform.localPosition.z;
        yOffset = transform.localPosition.y;
    }

    void Update () {
        var targetPos = bird.transform.position + Vector3.up * yOffset + bird.transform.forward * zOffset;
        transform.position = Vector3.Lerp(transform.position, targetPos, followSpeed * Time.deltaTime);

        var targetForward = bird.transform.forward;
        transform.forward = Vector3.Lerp(transform.forward, targetForward, followSpeed * Time.deltaTime);
	}
}
