﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdAnimator : MonoBehaviour {
    public bool Flapping {
        get {
            return flapping;
        }
        set {
            if (flapping == false && value == true) {
                time = 0;
            }
            flapping = value;
        }
    }
    private bool flapping = true;

    private float time;

    public float flapAmplitude = 45f;
    public float flapPeriod = 2;
    public float flapPhase = 0;

    public GameObject wing1;
    public GameObject wing2;
    public float resetSpeed = 1;
    	
	void Update () {
		if (flapping) {
            time += Time.deltaTime;

            float x = Mathf.PI * 2 * (time + flapPhase) / flapPeriod;
            var val = Mathf.Sin(x);
            val *= flapAmplitude;

            wing1.transform.localRotation = Quaternion.Euler(0, 0, val);
            wing2.transform.localRotation = Quaternion.Euler(0, 0, -val);
        } else {
            wing1.transform.localRotation = Quaternion.Lerp(wing1.transform.localRotation, Quaternion.identity, resetSpeed * Time.deltaTime);
            wing2.transform.localRotation = Quaternion.Lerp(wing2.transform.localRotation, Quaternion.identity, resetSpeed * Time.deltaTime);
        }
	}
}
