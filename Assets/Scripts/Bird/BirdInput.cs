﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdInput : MonoBehaviour {
    private BirdAnimator animator;

    private void Start() {
        animator = GetComponent<BirdAnimator>();
    }

    void Update () {
        animator.Flapping = Input.GetKey(KeyCode.Space);
	}
}
