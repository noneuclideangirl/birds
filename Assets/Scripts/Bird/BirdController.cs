﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour {
    private BirdAnimator animator;

    public float flapSpeed = 30;
    public float flapVerticalSpeed = 5;
    public float glideVerticalSpeed = -2;
    public float glideSpeed = 20;
    public float speedSmoothing = 1;
    public float rotationSmoothing = 10;
    public float maxRotation = 60;
    public float turnAngle = 20;
    public float lift = 0.5f;
    public float maxPitch = 70;
    public float maxRoll = 60;

    private float speed;
    private float slowFactor = 1;

    private float roll = 0;
    private float cumulativeRoll = 0;
    private float pitch = 0;

    
	void Start () {
        animator = GetComponent<BirdAnimator>();
	}
	
	void Update () {

        float targetSpeed = glideSpeed;
        float targetPitch = 0;
        float targetRoll = 0;
        float targetSlowFactor = 1;

		if (animator.Flapping) {
            targetSpeed = flapSpeed;
            transform.position += transform.up * flapVerticalSpeed * Time.deltaTime;
        } else {
            transform.position += Vector3.up * glideVerticalSpeed * Time.deltaTime;
        }
        transform.position += transform.up * lift * Time.deltaTime;

        targetRoll = -maxRotation * Input.GetAxis("Horizontal");
        targetPitch = turnAngle * Input.GetAxis("Vertical");

        speed = Mathf.Lerp(speed, targetSpeed, speedSmoothing * Time.deltaTime);
        slowFactor = Mathf.Lerp(slowFactor, targetSlowFactor, speedSmoothing * Time.deltaTime);
        
        var velocity = transform.forward * speed * Time.deltaTime * slowFactor;

        transform.position += velocity;

        pitch = Mathf.Lerp(pitch, targetPitch, rotationSmoothing);
        
        roll = Mathf.Lerp(roll, targetRoll, rotationSmoothing);

        //transform.rotation = Quaternion.identity;
        transform.Rotate(transform.right, pitch * Time.deltaTime, Space.World);
        transform.Rotate(transform.forward, roll * Time.deltaTime, Space.World);
    }
}
